Speechwriter, ghostwriter, and public speaking coach. Speeches written include: keynote speeches, awards speeches, graduation speeches, wedding toasts, retirement speeches. Written pieces include op-eds, guest magazine articles, and nonfiction business/Big Idea book projects.

Website: https://www.ricespeechwriting.com
